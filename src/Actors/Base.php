<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/31/16
 * Time: 8:31 AM
 */

namespace Smorken\Sanitizer\Actors;

use Illuminate\Support\Str;
use Smorken\Sanitizer\Contracts\Actor;
use Smorken\Sanitizer\SanitizerException;

abstract class Base implements Actor
{
    public function __call(string $name, array $params): mixed
    {
        array_unshift($params, $name);

        return call_user_func_array($this->sanitize(...), $params);
    }

    /**
     * @throws SanitizerException
     */
    public function sanitize(string $type, mixed $value): mixed
    {
        $m = Str::camel($type);
        if (method_exists($this, $m)) {
            $args = func_get_args();
            unset($args[0]);

            return call_user_func_array([$this, $m], $args);
        }
        throw new SanitizerException("$type cannot be sanitized.");
    }

    protected function alpha($value): string
    {
        return $this->preg($value, '/[^\p{L}]/');
    }

    protected function alphaNum($value): string
    {
        return $this->preg($value, '/[^\p{L}\p{N}]/');
    }

    protected function alphaNumDash($value): string
    {
        return $this->preg($value, '/[^\p{L}\p{N}\-_]/');
    }

    protected function alphaNumDashSpace($value): string
    {
        return $this->preg($value, '/[^\p{L}\p{N}\-_ ]/');
    }

    protected function bladeViewName($value): string
    {
        return $this->preg($value, '/[^A-Za-z0-9\-_\.]/');
    }

    protected function bool($value): bool
    {
        return (bool) $value;
    }

    protected function boolean($value): bool
    {
        return $this->bool($value);
    }

    protected function email($value): ?string
    {
        return filter_var($value, FILTER_SANITIZE_EMAIL);
    }

    protected function float($value): float
    {
        return (float) $value;
    }

    protected function int($value): int
    {
        return (int) filter_var($value, FILTER_SANITIZE_NUMBER_INT);
    }

    protected function integer($value): int
    {
        return $this->int($value);
    }

    protected function phpClassName($value): string
    {
        return $this->preg($value, '/[^A-Za-z0-9_\\\]/');
    }

    protected function preg($value, $regex): string
    {
        return preg_replace($regex, '', $value);
    }

    protected function purify($value): string
    {
        return $this->sanitize('xssAdmin', $value);
    }

    protected function string($value): string
    {
        return htmlentities(stripslashes(trim((string) $value)), ENT_QUOTES, 'UTF-8', false);
    }

    /**
     * Note that this method is NOT guaranteed to be safe!
     *
     * @param  array|string  $tags
     */
    protected function stripTags($value, $tags): mixed
    {
        if (is_string($tags)) {
            $tags = [$tags];
        }
        foreach ($tags as $tag) {
            $pattern = sprintf('/<%s.*>.*<\/%s>/', $tag, $tag);
            $value = $this->preg($value, $pattern);
        }

        return $value;
    }

    protected function url($value): ?string
    {
        return filter_var($value, FILTER_SANITIZE_URL);
    }
}
