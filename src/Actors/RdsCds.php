<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/31/16
 * Time: 8:40 AM
 */

namespace Smorken\Sanitizer\Actors;

class RdsCds extends Base
{
    protected function acadOrg($value): string
    {
        return $this->alphaNum($value);
    }

    protected function collegeId($value): string
    {
        return $this->alphaNum($value);
    }

    protected function comments($value): string
    {
        return $this->string($value);
    }

    protected function courseId($value): string
    {
        return $this->alphaNumDash($value);
    }

    protected function credits($value): int
    {
        return $this->int($value);
    }

    protected function detailId($value): string
    {
        return $this->alphaNumDash($value);
    }

    protected function diff($value): float
    {
        return $this->float($value);
    }

    protected function groupId($value): string
    {
        return $this->alphaNumDash($value);
    }

    protected function load($value): ?string
    {
        $valid = ['N', 'L', 'H', 'T', 'F'];
        if (in_array($value, $valid, true)) {
            return $value;
        }

        return null;
    }

    protected function meidOrId($value): string
    {
        return $this->alphaNum($value);
    }

    protected function name($value): string
    {
        return $this->string($value);
    }

    protected function page($value): int
    {
        return $this->int($value);
    }

    protected function planCode($value): string
    {
        return $this->alphaNum($value);
    }

    protected function studentId($value): int
    {
        return $this->int($value);
    }

    protected function termId($value): int
    {
        return $this->int($value);
    }
}
