<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/21/16
 * Time: 11:38 AM
 */

namespace Smorken\Sanitizer\Actors;

/**
 * Class Xss
 *
 * @url https://github.com/drupal/drupal/blob/9.3.x/core/lib/Drupal/Component/Utility/Xss.php
 *
 * @license GPL v3
 */
class Xss extends Base
{
    public static function filter($string, ?array $html_tags = null)
    {
        return \Smorken\Sanitizer\Drupal\Component\Utility\Xss::filter($string, $html_tags);
    }

    public static function filterAdmin($string)
    {
        return \Smorken\Sanitizer\Drupal\Component\Utility\Xss::filterAdmin($string);
    }

    public function xss($string, ?array $html_tags = null)
    {
        return Xss::filter($string, $html_tags);
    }

    public function xssAdmin($string)
    {
        return Xss::filterAdmin($string);
    }
}
