<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/31/16
 * Time: 8:32 AM
 */

namespace Smorken\Sanitizer\Contracts;

interface Actor
{
    public function sanitize(string $type, mixed $value): mixed;
}
