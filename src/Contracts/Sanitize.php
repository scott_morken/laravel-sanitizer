<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/31/16
 * Time: 8:33 AM
 */

namespace Smorken\Sanitizer\Contracts;

/**
 * Interface Sanitize
 *
 *
 * @method string alpha($value)
 * @method string alphaNum($value)
 * @method string alphaNumDash($value)
 * @method string alphaNumDashSpace($value)
 * @method string bladeViewName($value)
 * @method bool bool($value)
 * @method bool boolean($value)
 * @method string email($value)
 * @method float float($value)
 * @method int int($value)
 * @method int integer($value)
 * @method string phpClassName($value)
 * @method string preg($value, $regex)
 * @method string purify($value)
 * @method string string($value)
 * @method string stripTags($value)
 * @method string url($value)
 * @method string acadOrg($value)
 * @method string collegeId($value)
 * @method string comments($value)
 * @method string courseId($value)
 * @method int credits($value)
 * @method string detailId($value)
 * @method float diff($value)
 * @method string groupId($value)
 * @method string|null load($value)
 * @method string meidOrId($value)
 * @method string name($value)
 * @method int page($value)
 * @method string planCode($value)
 * @method int studentId($value)
 * @method int termId($value)
 * @method xss($value)
 * @method xssAdmin($value)
 */
interface Sanitize
{
    public function add(string $name, Actor $sanitizer): void;

    public function exists(string $name): bool;

    /**
     * @throws \Smorken\Sanitizer\SanitizerException
     */
    public function get(?string $sanitizer = null): Actor;

    /**
     * Call a sanitizer directly, defaults to the default sanitizer
     */
    public function sanitize(string $type, mixed $value, ?string $sanitizer = null): mixed;
}
