<?php

namespace Smorken\Sanitizer\Drupal\Component\Utility;

class UrlHelper
{
    /**
     * The list of allowed protocols.
     *
     * @var array
     */
    protected static $allowedProtocols = ['http', 'https'];

    /**
     * Processes an HTML attribute value and strips dangerous protocols from URLs.
     *
     * @param  string  $string
     *                          The string with the attribute value.
     * @return string
     *                Cleaned up and HTML-escaped version of $string.
     */
    public static function filterBadProtocol($string)
    {
        // Get the plain text representation of the attribute value (i.e. its
        // meaning).
        $string = Html::decodeEntities($string);

        return Html::escape(static::stripDangerousProtocols($string));
    }

    /**
     * Strips dangerous protocols (for example, 'javascript:') from a URI.
     *
     * This function must be called for all URIs within user-entered input prior
     * to being output to an HTML attribute value. It is often called as part of
     * \Drupal\Component\Utility\UrlHelper::filterBadProtocol() or
     * \Drupal\Component\Utility\Xss::filter(), but those functions return an
     * HTML-encoded string, so this function can be called independently when the
     * output needs to be a plain-text string for passing to functions that will
     * call Html::escape() separately. The exact behavior depends on the value:
     * - If the value is a well-formed (per RFC 3986) relative URL or
     *   absolute URL that does not use a dangerous protocol (like
     *   "javascript:"), then the URL remains unchanged. This includes all
     *   URLs generated via Url::toString().
     * - If the value is a well-formed absolute URL with a dangerous protocol,
     *   the protocol is stripped. This process is repeated on the remaining URL
     *   until it is stripped down to a safe protocol.
     * - If the value is not a well-formed URL, the same sanitization behavior as
     *   for well-formed URLs will be invoked, which strips most substrings that
     *   precede a ":". The result can be used in URL attributes such as "href"
     *   or "src" (only after calling Html::escape() separately), but this may not
     *   produce valid HTML (for example, malformed URLs within "href" attributes
     *   fail HTML validation). This can be avoided by using
     *   Url::fromUri($possibly_not_a_url)->toString(), which either throws an
     *   exception or returns a well-formed URL.
     *
     * @param  string  $uri
     *                       A plain-text URI that might contain dangerous protocols.
     * @return string
     *                A plain-text URI stripped of dangerous protocols. As with all plain-text
     *                strings, this return value must not be output to an HTML page without
     *                being sanitized first. However, it can be passed to functions
     *                expecting plain-text strings.
     *
     * @see \Drupal\Component\Utility\Html::escape()
     * @see \Drupal\Core\Url::toString()
     * @see \Drupal\Core\Url::fromUri()
     */
    public static function stripDangerousProtocols($uri)
    {
        $allowed_protocols = array_flip(static::$allowedProtocols);

        // Iteratively remove any invalid protocol found.
        do {
            $before = $uri;
            $colonpos = strpos($uri, ':');
            if ($colonpos > 0) {
                // We found a colon, possibly a protocol. Verify.
                $protocol = substr($uri, 0, $colonpos);
                // If a colon is preceded by a slash, question mark or hash, it cannot
                // possibly be part of the URL scheme. This must be a relative URL, which
                // inherits the (safe) protocol of the base document.
                if (preg_match('![/?#]!', $protocol)) {
                    break;
                }
                // Check if this is a disallowed protocol. Per RFC2616, section 3.2.3
                // (URI Comparison) scheme comparison must be case-insensitive.
                if (! isset($allowed_protocols[strtolower($protocol)])) {
                    $uri = substr($uri, $colonpos + 1);
                }
            }
        } while ($before != $uri);

        return $uri;
    }
}
