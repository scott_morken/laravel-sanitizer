<?php

namespace Smorken\Sanitizer;

class Factory
{
    protected static \Smorken\Sanitizer\Contracts\Sanitize $sanitize;

    public static function __callStatic(string $name, array $arguments)
    {
        return call_user_func_array([self::getSanitize(), $name], $arguments);
    }

    public static function getSanitize(): \Smorken\Sanitizer\Contracts\Sanitize
    {
        return self::$sanitize;
    }

    public static function setSanitize(\Smorken\Sanitizer\Contracts\Sanitize $sanitize): void
    {
        self::$sanitize = $sanitize;
    }
}
