<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/31/16
 * Time: 8:30 AM
 */

namespace Smorken\Sanitizer;

use Illuminate\Support\Arr;
use Smorken\Sanitizer\Contracts\Actor;

class Sanitize implements \Smorken\Sanitizer\Contracts\Sanitize
{
    protected string $default = 'standard';

    protected array $sanitizers = [];

    public function __construct(array $options)
    {
        $this->setupSanitizers($options);
    }

    /**
     * @throws \Smorken\Sanitizer\SanitizerException
     */
    public function __call(string $name, array $params): mixed
    {
        $sanitizer = null;
        $args = [$name];
        if (isset($params[1]) && $this->exists($params[1])) {
            $sanitizer = $params[1];
            unset($params[1]);
        }
        $args = array_merge($args, $params);

        return $this->sanitizeCall($sanitizer, $args);
    }

    /**
     * @throws \Smorken\Sanitizer\SanitizerException
     */
    public function __get(string $name): Actor
    {
        return $this->get($name);
    }

    public function add(string $name, Actor $sanitizer): void
    {
        $this->sanitizers[$name] = $sanitizer;
    }

    public function exists(string $name): bool
    {
        return array_key_exists($name, $this->sanitizers);
    }

    /**
     * @throws \Smorken\Sanitizer\SanitizerException
     */
    public function get(?string $sanitizer = null): Actor
    {
        if ($sanitizer === null) {
            $sanitizer = $this->default;
        }
        if ($this->exists($sanitizer)) {
            return $this->sanitizers[$sanitizer];
        }
        throw new SanitizerException("Unable to locate $sanitizer.");
    }

    /**
     * Call a sanitizer directly, defaults to the default sanitizer
     *
     * @throws SanitizerException
     */
    public function sanitize(string $type, mixed $value, ?string $sanitizer = null): mixed
    {
        $args = func_get_args();
        unset($args[2]);

        return $this->sanitizeCall($sanitizer, $args);
    }

    protected function sanitizeCall(?string $sanitizer, array $params): mixed
    {
        foreach ($this->sanitizers as $s) {
            try {
                return call_user_func_array([$s, 'sanitize'], $params);
            } catch (SanitizerException) {
                continue;
            }
        }
        throw new SanitizerException('Unable to sanitize '.$params[0]);
    }

    protected function setupSanitizers(array $options): void
    {
        $this->default = Arr::get($options, 'default', 'standard');
        $sans = Arr::get($options, 'sanitizers', []);
        foreach ($sans as $k => $s) {
            $this->add($k, new $s);
        }
    }
}
