<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/27/16
 * Time: 11:11 AM
 */

namespace Smorken\Sanitizer;

use Illuminate\Support\ServiceProvider as SP;
use Smorken\Sanitizer\Contracts\Sanitize;

class ServiceProvider extends SP
{
    public function boot(): void
    {
        $this->bootConfig();
        Factory::setSanitize($this->app[Sanitize::class]);
    }

    public function register(): void
    {
        $this->app->bind(Sanitize::class, fn ($app) => new \Smorken\Sanitizer\Sanitize($app['config']->get('sanitizer', [])));
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $app_path = config_path('sanitizer.php');
        $this->mergeConfigFrom($config, 'sanitizer');
        $this->publishes([$config => $app_path], 'config');
    }
}
