<?php

namespace Smorken\Sanitizer\Traits;

use Illuminate\Support\Arr;

trait SanitizeRequest
{
    public function sanitize(\Smorken\Sanitizer\Contracts\Sanitize $sanitize, $request, array $rules)
    {
        $new = [];
        foreach ($rules as $input => $rule) {
            $value = $this->sanitizeRule($sanitize, $this->getValueFromRequest($request, $input),
                is_string($rule) ? explode('|', $rule) : $rule);
            Arr::set($new, $input, $value);
        }
        if ($new) {
            $this->replaceRequestValues($request, $new);
        }

        return $request;
    }

    protected function getValueFromRequest($request, string $key): mixed
    {
        $data = $request;
        if (is_object($request)) {
            if (method_exists($request, 'validationData')) {
                $data = $request->validationData();
            } elseif (method_exists($request, 'all')) {
                $data = $request->all();
            }
        }

        return Arr::get($data, $key);
    }

    protected function replaceRequestValues(&$request, $values): void
    {
        if (is_object($request) && method_exists($request, 'replace')) {
            $request->replace(array_replace_recursive($request->all(), $values));
        } else {
            $request = array_replace_recursive($request, $values);
        }
    }

    protected function sanitizeRule(\Smorken\Sanitizer\Contracts\Sanitize $sanitize, $value, $rule): mixed
    {
        if (is_array($rule)) {
            foreach ($rule as $r) {
                $value = $this->sanitizeRule($sanitize, $value, $r);
            }
        } elseif (is_callable($rule)) {
            $value = $rule($value);
        } else {
            $value = $sanitize->$rule($value);
        }

        return $value;
    }
}
