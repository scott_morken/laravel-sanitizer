<?php

namespace Tests\Smorken\Sanitizer\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Sanitizer\Factory;

class FactoryTest extends TestCase
{
    public function testAcadOrgFromSis(): void
    {
        $t = '123abc-_ ';
        $this->assertEquals('123abc', Factory::acadOrg($t));
    }

    public function testStringFromStandard(): void
    {
        $t = '<script>alert();</script>';
        $this->assertEquals('&lt;script&gt;alert();&lt;/script&gt;', Factory::string($t));
    }

    public function testXssFromXss(): void
    {
        $t = '<script>alert(0)</script>';
        $this->assertEquals('alert(0)', Factory::xss($t));
    }

    protected function getOptions(): array
    {
        return [
            'default' => 'standard',
            'sanitizers' => [
                'standard' => \Smorken\Sanitizer\Actors\Standard::class,
                'sis' => \Smorken\Sanitizer\Actors\RdsCds::class,
                'xss' => \Smorken\Sanitizer\Actors\Xss::class,
            ],
        ];
    }

    protected function getSanitize(): Sanitize
    {
        return new \Smorken\Sanitizer\Sanitize($this->getOptions());
    }

    protected function setUp(): void
    {
        parent::setUp();
        Factory::setSanitize($this->getSanitize());
    }
}
