<?php

namespace Tests\Smorken\Sanitizer\Unit;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use PHPUnit\Framework\TestCase;
use Smorken\Sanitizer\Sanitize;
use Smorken\Sanitizer\Traits\SanitizeRequest;

class SanitizeRequestTest extends TestCase
{
    use SanitizeRequest;

    public function testArrayRule()
    {
        $request = new Request(['bar' => ' <?php foo bar; > ']);
        $request->setRouteResolver(
            function () use ($request) {
                return (new Route('GET', '', []))->bind($request);
            }
        );
        self::sanitize(new Sanitize($this->getOptions()), $request, ['bar' => ['trim', 'string']]);
        $this->assertEquals('&lt;?php foo bar; &gt;', $request->input('bar'));
    }

    public function testCallableRule()
    {
        $request = new Request(['bar' => 'fiz']);
        $request->setRouteResolver(
            function () use ($request) {
                return (new Route('GET', '', []))->bind($request);
            }
        );
        self::sanitize(
            new Sanitize($this->getOptions()),
            $request,
            [
                'bar' => function ($value) {
                    return sprintf('callback %s', $value);
                },
            ]
        );
        $this->assertEquals('callback fiz', $request->input('bar'));
    }

    public function testComplexRules()
    {
        $request = new Request(['arr' => ' <?php foo bar; > ', 'callable' => 'fiz', 'simple' => '123']);
        $request->setRouteResolver(
            function () use ($request) {
                return (new Route('GET', '', []))->bind($request);
            }
        );
        self::sanitize(
            new Sanitize($this->getOptions()),
            $request,
            [
                'arr' => ['trim', 'string'],
                'callable' => function ($value) {
                    return sprintf('callback %s', $value);
                },
                'simple' => 'int',
            ]
        );
        $this->assertEquals(
            ['arr' => '&lt;?php foo bar; &gt;', 'callable' => 'callback fiz', 'simple' => '123'],
            $request->all()
        );
    }

    public function testSimpleRule()
    {
        $request = new Request(['bar' => '123foo']);
        $request->setRouteResolver(
            function () use ($request) {
                return (new Route('GET', '', []))->bind($request);
            }
        );
        self::sanitize(new Sanitize($this->getOptions()), $request, ['bar' => 'int']);
        $this->assertEquals('123', $request->input('bar'));
    }

    public function testSimpleRuleWithDotNotation()
    {
        $request = new Request(['foo' => ['bar' => '123foo']]);
        $request->setRouteResolver(
            function () use ($request) {
                return (new Route('GET', '', []))->bind($request);
            }
        );
        self::sanitize(new Sanitize($this->getOptions()), $request, ['foo.bar' => 'int']);
        $this->assertEquals('123', $request->input('foo.bar'));
    }

    protected function getOptions()
    {
        return [
            'default' => 'standard',
            'sanitizers' => [
                'standard' => \Smorken\Sanitizer\Actors\Standard::class,
            ],
        ];
    }
}
